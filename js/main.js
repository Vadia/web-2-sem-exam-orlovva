const methods = {
	data() {
		return {
			isActive: false
		}
	},
	methods: {
		tc() {
			this.isActive = !this.isActive;	
		}	
	}
};

new Vue({
	el: '.all',
	mixins: [methods]
});